const en = require("./en.json");
const ru = require("./ru.json");
const ua = require("./ua.json");
const es = require("./es.json");
const zh = require("./zh.json");
const lv = require("./lv.json");
const pt = require("./pt.json");
const fr = require("./fr.json");
const it = require("./it.json");
const bg = require("./bg.json");
const et = require("./et.json");
const lt = require("./lt.json");
const de = require("./de.json");
const pl = require("./pl.json");

module.exports = {
  en,
  ru,
  ua,
  es,
  zh,
  lv,
  pt,
  fr,
  it,
  bg,
  et,
  lt,
  de,
  pl
};
